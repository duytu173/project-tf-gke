variable "project" {
  default = "inbound-decker-321207"
}

variable "region" {
  default = "us-east1"
}

variable "credentials" {
  default = "account-tf-gke.json"
}

variable "machine_type" {
  default = "g1-small"
}

variable "cluster" {
  default = "terraform-cluster"
}
variable "app" {
  type        = string
  description = "Name of application"
  default     = "cicd-101"
}
variable "zone" {
  default = "us-east1-d"
}
variable "docker-image" {
  type        = string
  description = "name of the docker image to deploy"
  default     = "ariv3ra/learniac:latest"
}